import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Redirect, Switch} from 'react-router-dom';
import App from './App';
import Register from './Screens/Register';
import Activate from './Screens/Activate';
import Login from './Screens/Login';
import Forget from './Screens/Forget';
import Reset from './Screens/Reset';
import Private from './Screens/Private.jsx';
import Admin from './Screens/Admin.jsx';


import 'react-toastify/dist/ReactToastify.css';

ReactDOM.render(
	<BrowserRouter>
		<Switch>
      		<Route path='/' exact render={props => <App {...props} />}/>
	 		<Route path='/register' exact render={props => <Register {...props} />}/>
	 		<Route path='/login' exact render={props => <Login {...props} />}/>
	 		<Route path='/users/password/forget' exact render={props => <Forget {...props} />}/>
	 		<Route path='/users/activate/:token' exact render={props => <Activate {...props} />}/>
	 		<Route path='/users/password/reset/:token' exact render={props => <Reset {...props} />}/>
    		
    		<Private path="/private" exact component={Private} />
       		<Admin path="/admin" exact component={Admin} />
    
    	</Switch>
  	</BrowserRouter>,
  document.getElementById('root')
);